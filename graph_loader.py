import pathlib
import ast
from graph import Graph


def load_graph(file_name):
    print('START LOADING TXT')
    graph = Graph()
    path = str(pathlib.Path(__file__).parent.resolve()) + '/' + file_name + '.txt'
    with open(path) as file:
        lines = file.readlines()
        for line in lines:
            if line in [' ', '\n']:
                continue
            coordinates = line.split(' ')
            graph.add_edge(coordinates[0], coordinates[1])
    print('LOADING FINISHED')
    return graph


def load_ways(file_name):
    print('START LOADING TXT')
    all_ways = []
    cities = []
    path = str(pathlib.Path(__file__).parent.resolve()) + '/' + file_name + '.txt'
    with open(path) as file:
        lines = file.readlines()
        for line in lines:
            if line in [' ', '\n']:
                continue
            coordinates = line.split(' ')
            way = []
            for city_coordinates in coordinates:
                if city_coordinates in [' ', '\n']:
                    continue
                city = ast.literal_eval(city_coordinates)
                try:
                    city_index = cities.index(city)
                except:
                    cities.append(city)
                    city_index = len(cities) - 1
                way.append(city_index)
            all_ways.append(way)
    print('LOADING FINISHED')
    return all_ways, cities
