from graph_loader import load_ways, load_graph
from graph import Graph

graph: Graph = load_graph('graph1')
if len(graph.edges) < 50:
    print('EDGES: ', graph.edges)
else:
    print('EDGE COUNT: ', len(graph.edges))

if len(graph.vertices) < 50:
    print('VERTICES: ', graph.vertices)
else:
    print('VERTEX COUNT: ', len(graph.vertices))

print('CONNECTED COMPONENTS COUNT', graph.get_connected_components())

if len(graph.parent) < 50:
    print('PARENT: ', graph.parent)
else:
    print('PARENT COUNT: ', len(graph.parent))

graph.connect_all_components_v2()
graph.show_results()
