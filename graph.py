import ast
import math
from pandas import *
from scipy import spatial


class Graph:
    def __init__(self):
        self.vertices = []
        self.edges = []
        self.parent = []
        self.unique_parents = []
        self.component_vertices = []
        self.main_component = []
        self.new_edges = []

    def add_edge(self, vertex1, vertex2):
        """Add new edge - link between cities"""
        vertex_index1 = self.add_vertex(ast.literal_eval(vertex1))
        vertex_index2 = self.add_vertex(ast.literal_eval(vertex2))
        self.edges.append([vertex_index1, vertex_index2])

    def add_vertex(self, vertex):
        """Add vertex - city"""
        try:
            vertex_index = self.vertices.index(vertex)
        except:
            self.vertices.append(vertex)
            vertex_index = len(self.vertices) - 1
        return vertex_index

    def get_connected_components(self):
        """Create connected components"""
        print('\n\n----------------------------------------\nSTART CALCULATE CONNECTED COMPONENTS')
        self.parent = [i for i in range(len(self.vertices))]
        for edge in self.edges:
            self.connect(edge[0], edge[1])
        print('FINISH CALCULATE CONNECTED COMPONENTS\n---------------------------------------\n\n')

        self.mapify_parents()
        return len(self.unique_parents)

    def connect(self, vertex1, vertex2):
        """Connect 2 vertices in edge"""

        # find roots of both vertices
        vertex1_root = self.find_root(vertex1)
        vertex2_root = self.find_root(vertex2)

        # if they do not have same root, root of vertex1 will become parent of vertex2's root
        if vertex1_root != vertex2_root:
            self.parent[vertex2_root] = vertex1_root

    def find_root(self, vertex):
        """Will find oldest parent of vertex"""
        # If tested vertex is root
        if vertex == self.parent[vertex]:
            return vertex

        # Set root of it's parent as vertex's root
        self.parent[vertex] = self.find_root(self.parent[vertex])
        return self.parent[vertex]

    def mapify_parents(self):
        self.unique_parents = []
        self.component_vertices = []

        for p_i in range(len(self.parent)):
            p = self.parent[p_i]
            if p not in self.unique_parents:
                self.unique_parents.append(p)
                self.component_vertices.append([self.vertices[p_i]])
            else:
                i = self.unique_parents.index(p)
                self.component_vertices[i].append(self.vertices[p_i])
        self.main_component = self.component_vertices.pop(0)






    def connect_all_components_v1(self):
        print('\n\n----------------------------------------\nSTART CONNECTING DIVIDED COMPONENTS')
        if len(self.parent) == 0:
            self.parent = self.get_connected_components()
        self.connect_components_to_main_component_v1()
        print('FINISH CALCULATION OF DISTANCES MATRIX AND START CONNECTING THEM')
        print('FINISH CONNECTING DIVIDED COMPONENTS\n---------------------------------------\n\n')

    def connect_components_to_main_component_v1(self):
        while len(self.component_vertices) > 0:
            print('main component size: ', len(self.main_component), ' ----///--- number of other components ', len(self.component_vertices))
            best_distance = -1
            best_component_index = -1
            best_edge = [[-1, -1], [-1, -1]]
            for component_index in range(len(self.component_vertices)):
                component = self.component_vertices[component_index]
                distance, edge = self.get_best_distance_v1(self.main_component, component)
                if best_distance == -1 or distance < best_distance:
                    best_distance = distance
                    best_component_index = component_index
                    best_edge = edge
            chosen_component = self.component_vertices.pop(best_component_index)
            self.main_component += chosen_component
            self.new_edges.append(best_edge)

    def get_best_distance_v1(self, vertices1, vertices2):
        best_distance = -1
        best_edge = [[-1, -1], [-1, -1]]

        for vertex1 in vertices1:
            for vertex2 in vertices2:
                distance = math.dist(vertex1, vertex2)
                if best_distance == -1 or distance < best_distance:
                    best_distance = distance
                    best_edge = [vertex1, vertex2]

        return best_distance, best_edge








    def connect_all_components_v2(self):
        print('\n\n----------------------------------------\nSTART CONNECTING DIVIDED COMPONENTS')
        if len(self.parent) == 0:
            self.parent = self.get_connected_components()
        self.connect_components_to_main_component_v2()
        print('FINISH CALCULATION OF DISTANCES MATRIX AND START CONNECTING THEM')
        print('FINISH CONNECTING DIVIDED COMPONENTS\n---------------------------------------\n\n')

    def connect_components_to_main_component_v2(self):
        self.main_component = []
        self.move_component_vertices(min(self.parent))
        while len(self.vertices) > 0:
            print('main component size: ', len(self.main_component), ' ----///--- other components together size ', len(self.vertices))
            edge, index = self.get_best_distance_v2(self.main_component, self.vertices)
            self.new_edges.append(edge)
            self.move_component_vertices(self.parent[index])

    def move_component_vertices(self, component_index):
        for i in reversed(range(len(self.vertices))):
            if self.parent[i] == component_index:
                self.main_component.append(self.vertices.pop(i))
                self.parent.pop(i)

    def get_best_distance_v2(self, vertices1, vertices2):
        best_distance = -1
        best_edge = [[-1, -1], [-1, -1]]
        best_vertex_index = -1
        results = spatial.KDTree(vertices2).query(vertices1)
        for i in range(len(results[0])):
            distance = results[0][i]
            if best_distance == -1 or distance < best_distance:
                best_distance = distance
                vertex1 = vertices1[i]
                vertex2_index = results[1][i]
                vertex2 = vertices2[vertex2_index]
                best_edge = [vertex1, vertex2]
                best_vertex_index = vertex2_index

        return best_edge, best_vertex_index

    def show_results(self):
        print('NEW EDGES: ')
        for edge in self.new_edges:
            print('from ', edge[0], ' to ', edge[1])

